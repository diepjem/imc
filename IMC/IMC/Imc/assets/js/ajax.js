﻿/// <reference path="ajax.js" />

function servicioAjax(metodo, urlServicio, datos, FunctionRespuesta) {
    try {
        //console.log(" llego al metodo servicioAjax");
        jQuery.ajax(
            {
                url: urlServicio,
                data: datos,
                dataType: "json",
                type: metodo,
                async: false, // La petición es síncrona
                cache: false, // No queremos usar la caché del navegador
                contentType: "application/json; charset=utf-8",
                success: FunctionRespuesta,
                failure: function (response) {
                    //console.log(response.d);
                },
                error: function (response) {
                    //console.log(response);
                }
            });
    } catch (ex) { /*console.log(ex);*/}
}