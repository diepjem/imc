﻿Imports System.Web.UI
Imports System.Web.Services
Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient

Public Class servicio
    Inherits System.Web.UI.Page
    ''' <summary>
    ''' Inicia programa
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    <WebMethod>
    Public Shared Function AgregarDatos(id_clave_servidor As Integer, sexo As Integer, edad As Integer, peso As Double, altura As Integer, calculo As Double) As Boolean

        Dim estatus As Boolean = False
        Try
            Dim ConexionDB As New conexion()
            Dim conexxion = ConexionDB.conexion_global(1)
            'cmd.Parameters.AddWithValue("@fechaFin", fechaFin)
            Dim sql As String = "ingresa_imc"

            Dim cmd As New MySqlCommand(sql, conexxion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@_id_clave_servidor", id_clave_servidor)
            cmd.Parameters.AddWithValue("@_sexo", sexo)
            cmd.Parameters.AddWithValue("@_edad", edad)
            cmd.Parameters.AddWithValue("@_peso", peso)
            cmd.Parameters.AddWithValue("@_altura", altura)
            cmd.Parameters.AddWithValue("@_calculo", calculo)


            Dim reader As MySqlDataReader = cmd.ExecuteReader

            While reader.Read()
                estatus = CBool((reader("estatus").ToString))
                'Console.WriteLine(reader(0).ToString)
                'datosLlena.Add(New Usuario() With {
                '    .idUsuario = reader("idUsuario").ToString,
                '    .usuario = reader("usuario").ToString,
                '    .password = reader("pas").ToString
                '                    })


            End While

            ConexionDB.cerrar()

        Catch ex As Exception
            estatus = False
        End Try
        Return estatus
    End Function
End Class